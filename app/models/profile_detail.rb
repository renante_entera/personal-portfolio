class ProfileDetail < ApplicationRecord
  belongs_to :profile

  validates :title, :content, :sort_order, presence: true
end
