class WorkExperience < ApplicationRecord
  belongs_to :profile

  validates :job_title, :employer_name, :date_started, presence: true
  validates :description, presence: true, length: { minimum: 10, maximum: 300 }

  def work_period
    started = date_started.to_s(:month_and_year)
    ended = date_ended.present? ? date_ended.to_s(:month_and_year) : "Present"

    "#{started} to #{ended}"
  end
end
