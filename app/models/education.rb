class Education < ApplicationRecord
  belongs_to :profile

  validates :course, :school_name, :date_graduated, presence: true

  def date_graduated_display
    date_graduated.to_s(:long)
  end
end
