class PortfolioPhoto < ApplicationRecord
  belongs_to :portfolio

  validates :title, presence: true
  validates :description, presence: true, length: { minimum: 10, maximum: 100 }
end
