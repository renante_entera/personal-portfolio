class Service < ApplicationRecord
  after_validation :set_slug, only: %i[create update]

  belongs_to :profile

  validates :name, presence: true, uniqueness: true
  validates :description, presence: true, length: { minimum: 10, maximum: 300 }

  private

  def set_slug
    self.slug = [name.to_s.parameterize].join("-")
  end
end
