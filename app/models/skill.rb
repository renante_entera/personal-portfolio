class Skill < ApplicationRecord
  belongs_to :profile

  validates :skill_name, :proficiency, presence: true
end
