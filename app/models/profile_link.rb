class ProfileLink < ApplicationRecord
  belongs_to :profile

  validates :name, :url, presence: true
end
