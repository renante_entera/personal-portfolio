class Testimonial < ApplicationRecord
  belongs_to :profile

  validates :customer_name, presence: true
  validates :statement, presence: true, length: { minimum: 10, maximum: 300 }
end
