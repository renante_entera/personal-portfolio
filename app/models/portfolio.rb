class Portfolio < ApplicationRecord
  after_validation :set_slug, only: %i[create update]

  belongs_to :profile
  has_many :portfolio_photos, dependent: :destroy

  validates :title, presence: true, uniqueness: true
  validates :category, :project_date, presence: true
  validates :description, presence: true, length: { minimum: 10, maximum: 500 }

  def project_date_display
    project_date.to_s(:month_and_year)
  end

  private

  def set_slug
    self.slug = [title.to_s.parameterize].join("-")
  end
end
