class Profile < ApplicationRecord
  # after_validation :set_home_photo, only: %i[create update]
  # after_validation :set_profile_photo, only: %i[create update]

  has_many :profile_details, dependent: :destroy
  has_many :profile_links, dependent: :destroy
  has_many :work_experiences, dependent: :destroy
  has_many :skills, dependent: :destroy
  has_many :educations, dependent: :destroy
  has_many :services, dependent: :destroy
  has_many :portfolios, dependent: :destroy
  has_many :testimonials, dependent: :destroy

  validates :first_name, :last_name, presence: true

  def fullname
    "#{first_name} #{last_name}"
  end

  # private

  # def set_home_photo
  #   photo = Cloudinary::Uploader.upload(home_photo)
  #   self.home_photo = photo["url"]
  # end

  # def set_profile_photo
  #   photo = Cloudinary::Uploader.upload(profile_photo)
  #   self.profile_photo = photo["url"]
  # end
end
