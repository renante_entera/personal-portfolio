class ContactMailer < ApplicationMailer
  default from: "notifications@example.com"

  def new_contact_email
    @contact = params[:contact]

    mail(to: "renante.entera@gmail.com", subject: @contact.subject, reply_to: @contact.email)
  end
end
