require "administrate/field/base"

class IconField < Administrate::Field::Base
  def to_s
    data
  end

  def icon
    "<i class='bi bi-#{data}'></i>" if data.present?
  end
end
