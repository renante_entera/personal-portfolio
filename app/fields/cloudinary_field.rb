require "administrate/field/base"

class CloudinaryField < Administrate::Field::Base
  include CloudinaryHelper

  def thumb
    cl_image_tag(data, width: 100, crop: "pad") if data.present?
  end
end
