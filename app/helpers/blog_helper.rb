module BlogHelper
  def request_header
    {
      "Authorization" => "Bearer #{bearer_token}",
      "Content-Type"  => "application/json"
    }
  end

  def display_posts
    response = faraday_conn.get("posts")

    response.body
  end

  def show_post(slug)
    response = faraday_conn.get("posts/#{slug}")

    response.body["data"].presence
  end

  def create_post(body)
    response = faraday_conn.post("posts", body.to_json, request_header)

    response.body
  end

  def update_post(slug, body)
    response = faraday_conn.patch("posts/#{slug}", body.to_json, request_header)

    response.body
  end

  def delete_post(slug)
    response = faraday_conn.delete("posts/#{slug}", nil, request_header)

    response.body
  end
end
