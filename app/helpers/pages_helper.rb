module PagesHelper
  def user_posts
    response = faraday_conn.get("my-posts", nil,
                                "Authorization" => "Bearer #{bearer_token}")

    response.body
  end
end
