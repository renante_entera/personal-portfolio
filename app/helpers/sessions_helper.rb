module SessionsHelper
  def authenticate_user(body)
    response = faraday_conn.post("login", body.to_json,
                                 "Content-Type" => "application/json")

    response.body
  end
end
