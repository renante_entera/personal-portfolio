module UsersHelper
  def register_user(body)
    response = faraday_conn.post("users", body.to_json,
                                 "Content-Type" => "application/json")

    response.body
  end
end
