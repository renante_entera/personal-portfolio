require "faraday"
require "faraday_middleware"

module ApplicationHelper
  def active?(link_path)
    current_page?(link_path) ? "active" : ""
  end

  def current_profile
    Profile.first
  end

  def cloudinary_upload(photo)
    Cloudinary::Uploader.upload(photo)
  end

  def upload_photo(photo)
    uploaded_file = cloudinary_upload(photo)

    uploaded_file["url"]
  end

  def faraday_conn
    url = ENV["BLOG_API_URL"]

    Faraday.new(url: url) do |faraday|
      faraday.adapter Faraday.default_adapter
      faraday.response :json
    end
  end

  def logged_in?
    session[:current_user].present?
  end

  def current_user
    session[:current_user]["user"] if logged_in?
  end

  def bearer_token
    session[:current_user]["token"] if logged_in?
  end

  def post_owner?(post_user)
    logged_in? && current_user["id"] == post_user["id"]
  end
end
