class ServicesController < ApplicationController
  before_action :set_service, only: %i[show]

  def index
    @services = current_profile.services
    @testimonials = current_profile.testimonials
  end

  def show; end

  private

  def set_service
    @service = Service.find_by(slug: params[:slug])
  end
end
