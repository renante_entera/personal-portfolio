class Admin::ProfilesController < Admin::ApplicationController
  before_action :set_profile, only: %i[update]
  before_action :set_profile_params, only: %i[update]

  def update
    super
    @profile.update(@profile_params)
  end

  private

  def set_profile
    @profile = Profile.find_by(id: params[:id])
  end

  def set_profile_params
    @profile_params = resource_params

    if resource_params[:home_photo].present?
      @profile_params[:home_photo] = photo(resource_params[:home_photo])
    end

    return if resource_params[:profile_photo].nil?

    @profile_params[:profile_photo] = photo(resource_params[:profile_photo])
  end

  def photo(photo)
    uploaded_file = cloudinary_upload(photo)

    uploaded_file["url"]
  end
end
