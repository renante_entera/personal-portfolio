class Admin::TestimonialsController < Admin::ApplicationController
  before_action :set_testimonial, only: %i[update]
  before_action :set_testimonial_params, only: %i[update]

  def update
    super
    @testimonial.update(@testimonial_params)
  end

  private

  def set_testimonial
    @testimonial = Testimonial.find_by(id: params[:id])
  end

  def set_testimonial_params
    @testimonial_params = resource_params

    return if resource_params[:photo].nil?

    @testimonial_params[:photo] = upload_photo(resource_params[:photo])
  end
end
