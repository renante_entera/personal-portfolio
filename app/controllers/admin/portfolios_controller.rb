class Admin::PortfoliosController < Admin::ApplicationController
  before_action :set_portfolio, only: %i[update]
  before_action :set_portfolio_params, only: %i[update]

  def update
    super
    @portfolio.update(@portfolio_params)
  end

  private

  def set_portfolio
    @portfolio = Portfolio.find_by(id: params[:id])
  end

  def set_portfolio_params
    @portfolio_params = resource_params

    return if resource_params[:photo].nil?

    @portfolio_params[:photo] = upload_photo(resource_params[:photo])
  end
end
