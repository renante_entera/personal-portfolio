class Admin::PortfolioPhotosController < Admin::ApplicationController
  before_action :set_portfolio_photo, only: %i[update]
  before_action :set_portfolio_photo_params, only: %i[update]

  def update
    super
    @portfolio_photo.update(@portfolio_photo_params)
  end

  private

  def set_portfolio_photo
    @portfolio_photo = PortfolioPhoto.find_by(id: params[:id])
  end

  def set_portfolio_photo_params
    @portfolio_photo_params = resource_params

    return if resource_params[:photo].nil?

    @portfolio_photo_params[:photo] = upload_photo(resource_params[:photo])
  end
end
