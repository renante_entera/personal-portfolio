class ContactController < ApplicationController
  skip_before_action :verify_authenticity_token, only: %i[create]

  def index
    @contact_details = current_profile.profile_details.where(show_in_contact: true)
  end

  def create
    @contact = Contact.new(contact_params)
    @contact.ip_address = request.remote_ip

    return unless @contact.save

    ContactMailer.with(contact: @contact).new_contact_email.deliver_later
    render json: { "msg" => "OK" }
  end

  private

  def contact_params
    params.permit(:name, :email, :subject, :message)
  end
end
