class ResumeController < ApplicationController
  def index
    @skills = current_profile.skills.order("skills.proficiency desc, skills.skill_name")
    @educations = current_profile.educations
                                 .order("educations.date_graduated desc")
    @work_experiences = current_profile.work_experiences
                                       .order("work_experiences.date_started desc")
  end
end
