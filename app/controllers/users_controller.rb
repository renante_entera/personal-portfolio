class UsersController < ApplicationController
  include UsersHelper

  def new; end

  def create
    response = register_user(user_params)

    if response["message"].present?
      flash.now[:alert] = response
      render :new
    end

    return if response["data"].nil?

    flash[:notice] = "You have successfully registered and you may now use it to login!"
    redirect_to blog_index_path
  end

  private

  def user_params
    params.permit(:first_name, :last_name, :email, :password, :password_confirmation)
  end
end
