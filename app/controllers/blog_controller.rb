class BlogController < ApplicationController
  include BlogHelper
  before_action :authorized, except: %i[index show]
  before_action :set_post, only: %i[show edit]
  before_action :check_cancel, only: %i[create update]

  def index
    @posts = display_posts
  end

  def show; end

  def new; end

  def create
    response = create_post(post_params)

    if response["message"].present?
      flash.now[:alert] = response
      render :new
    end

    return if response["data"].nil?

    redirect_to my_posts_path,
                flash: { notice: "You have successfully created a new post!" }
  end

  def edit; end

  def update
    response = update_post(params[:slug], post_params)

    if response["message"].present?
      flash.now[:alert] = response
      render :show
    end

    return if response["data"].nil?

    redirect_to blog_path(response["data"]["to_param"]),
                flash: { notice: "You have successfully updated this post!" }
  end

  def destroy
    response = delete_post(params[:slug])

    if response["message"].present?
      flash.now[:alert] = response
      render :show
    end

    return if response["status"].nil?

    redirect_to my_posts_path,
                flash: { notice: "You have successfully deleted your post!" }
  end

  private

  def set_post
    @post = show_post(params[:slug])
  end

  def post_params
    params[:image] = upload_photo(params[:image]) if params[:image].present?

    params.permit(:title, :image, :content)
  end

  def check_cancel
    return unless params[:commit] == "Cancel"

    return redirect_to blog_path(params[:slug]) if params[:slug].present?

    redirect_to my_posts_path
  end
end
