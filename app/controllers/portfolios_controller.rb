class PortfoliosController < ApplicationController
  before_action :set_portfolio, only: %i[show]

  def index
    @portfolios = current_profile.portfolios
                                 .order("project_date desc")
  end

  def show; end

  private

  def set_portfolio
    @portfolio = Portfolio.find_by(slug: params[:slug])
  end
end
