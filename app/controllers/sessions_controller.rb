class SessionsController < ApplicationController
  include SessionsHelper

  def new; end

  def create
    response = authenticate_user(session_params)

    if response["message"].present?
      flash.now[:alert] = response
      render :new
    end

    return if response["token"].nil?

    session[:current_user] = response

    flash[:notice] = "You have successfully logged in!"
    redirect_to my_posts_path
  end

  def destroy
    reset_session

    flash[:notice] = "You have successfully logged out!"
    redirect_to blog_index_path
  end

  private

  def session_params
    params.permit(:email, :password)
  end
end
