class ApplicationController < ActionController::Base
  include ApplicationHelper

  def authorized
    return if logged_in?

    redirect_to blog_index_path,
                flash: { alert: { message: "You need to sign in or sign up before continuing." } }
  end

  def expired_token
    reset_session

    redirect_to login_path,
                flash: { alert: { message: "Your token has already expired.  Please log in." } }
  end
end
