class PagesController < ApplicationController
  include PagesHelper

  def my_posts
    @posts = user_posts

    expired_token unless @posts["message"].nil?

    return if @posts["message"].present?

    render "blog/index"
  end
end
