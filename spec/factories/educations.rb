FactoryBot.define do
  factory :education do
    course { "MyString" }
    school_name { "MyString" }
    honors_received { "MyString" }
    date_graduated { "2021-07-09" }
    profile { nil }
  end
end
