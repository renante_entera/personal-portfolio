FactoryBot.define do
  factory :contact do
    name { "MyString" }
    email { "MyString" }
    subject { "MyString" }
    message { "MyText" }
    ip_address { "MyString" }
  end
end
