FactoryBot.define do
  factory :service do
    name { "MyString" }
    description { "MyText" }
    profile { nil }
  end
end
