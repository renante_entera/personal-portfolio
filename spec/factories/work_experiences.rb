FactoryBot.define do
  factory :work_experience do
    job_title { "MyString" }
    description { "MyText" }
    employer_name { "MyString" }
    employer_address { "MyString" }
    date_started { "2021-07-08" }
    date_ended { "2021-07-08" }
    profile { nil }
  end
end
