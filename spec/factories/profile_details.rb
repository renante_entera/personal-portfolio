FactoryBot.define do
  factory :profile_detail do
    title { "MyString" }
    content { "MyString" }
    sort_order { 1 }
    profile { nil }
  end
end
