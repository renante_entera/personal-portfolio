FactoryBot.define do
  factory :portfolio_photo do
    title { "MyString" }
    description { "MyText" }
    photo { "MyString" }
    portfolio { nil }
  end
end
