FactoryBot.define do
  factory :portfolio do
    title { "MyString" }
    description { "MyText" }
    category { 1 }
    client { "MyString" }
    project_date { "2021-07-12" }
    project_url { "MyString" }
    photo { "MyString" }
    profile { nil }
  end
end
