FactoryBot.define do
  factory :skill do
    skill_name { "MyString" }
    proficiency { 1 }
    profile { nil }
  end
end
