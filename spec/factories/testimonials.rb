FactoryBot.define do
  factory :testimonial do
    customer_name { "MyString" }
    customer_position { "MyString" }
    statement { "MyText" }
    photo { "MyString" }
    profile { nil }
  end
end
