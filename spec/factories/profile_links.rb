FactoryBot.define do
  factory :profile_link do
    name { "MyString" }
    description { "MyText" }
    url { "MyString" }
    icon { "MyString" }
    sort_order { 1 }
    profile { nil }
  end
end
