class AddIndexToPortfolios < ActiveRecord::Migration[6.1]
  def change
    add_index :portfolios, [:title], :unique => true
  end
end
