class CreateSkills < ActiveRecord::Migration[6.1]
  def change
    create_table :skills do |t|
      t.string :skill_name
      t.integer :proficiency
      t.references :profile, null: false, foreign_key: true

      t.timestamps
    end
  end
end
