class CreateProfileDetails < ActiveRecord::Migration[6.1]
  def change
    create_table :profile_details do |t|
      t.string :title
      t.string :content
      t.integer :sort_order
      t.references :profile, null: false, foreign_key: true

      t.timestamps
    end
  end
end
