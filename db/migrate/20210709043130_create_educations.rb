class CreateEducations < ActiveRecord::Migration[6.1]
  def change
    create_table :educations do |t|
      t.string :course
      t.string :school_name
      t.string :honors_received
      t.date :date_graduated
      t.references :profile, null: false, foreign_key: true

      t.timestamps
    end
  end
end
