class AddSlugToPortfolios < ActiveRecord::Migration[6.1]
  def change
    add_column :portfolios, :slug, :string
  end
end
