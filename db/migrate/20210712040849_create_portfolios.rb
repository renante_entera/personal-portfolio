class CreatePortfolios < ActiveRecord::Migration[6.1]
  def change
    create_table :portfolios do |t|
      t.string :title
      t.text :description
      t.integer :category
      t.string :client
      t.date :project_date
      t.string :project_url
      t.string :photo
      t.references :profile, null: false, foreign_key: true

      t.timestamps
    end
  end
end
