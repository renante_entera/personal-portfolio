class CreateWorkExperiences < ActiveRecord::Migration[6.1]
  def change
    create_table :work_experiences do |t|
      t.string :job_title
      t.text :description
      t.string :employer_name
      t.string :employer_address
      t.date :date_started
      t.date :date_ended
      t.references :profile, null: false, foreign_key: true

      t.timestamps
    end
  end
end
