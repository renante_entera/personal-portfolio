class AddColumnsToProfileDetails < ActiveRecord::Migration[6.1]
  def change
    add_column :profile_details, :icon, :string
    add_column :profile_details, :show_in_contact, :boolean
  end
end
