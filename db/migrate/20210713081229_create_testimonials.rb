class CreateTestimonials < ActiveRecord::Migration[6.1]
  def change
    create_table :testimonials do |t|
      t.string :customer_name
      t.string :customer_position
      t.text :statement
      t.string :photo
      t.references :profile, null: false, foreign_key: true

      t.timestamps
    end
  end
end
