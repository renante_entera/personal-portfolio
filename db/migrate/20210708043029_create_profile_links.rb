class CreateProfileLinks < ActiveRecord::Migration[6.1]
  def change
    create_table :profile_links do |t|
      t.string :name
      t.text :description
      t.string :url
      t.string :icon
      t.integer :sort_order
      t.references :profile, null: false, foreign_key: true

      t.timestamps
    end
  end
end
