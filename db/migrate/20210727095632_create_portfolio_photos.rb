class CreatePortfolioPhotos < ActiveRecord::Migration[6.1]
  def change
    create_table :portfolio_photos do |t|
      t.string :title
      t.text :description
      t.string :photo
      t.references :portfolio, null: false, foreign_key: true

      t.timestamps
    end
  end
end
