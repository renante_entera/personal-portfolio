class AddPhotoToProfiles < ActiveRecord::Migration[6.1]
  def change
    add_column :profiles, :home_photo, :string
    add_column :profiles, :profile_photo, :string
  end
end
