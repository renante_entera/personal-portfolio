# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_08_18_041235) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contacts", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "subject"
    t.text "message"
    t.string "ip_address"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "educations", force: :cascade do |t|
    t.string "course"
    t.string "school_name"
    t.string "honors_received"
    t.date "date_graduated"
    t.bigint "profile_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["profile_id"], name: "index_educations_on_profile_id"
  end

  create_table "portfolio_photos", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.string "photo"
    t.bigint "portfolio_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["portfolio_id"], name: "index_portfolio_photos_on_portfolio_id"
  end

  create_table "portfolios", force: :cascade do |t|
    t.string "title"
    t.text "description"
    t.integer "category"
    t.string "client"
    t.date "project_date"
    t.string "project_url"
    t.string "photo"
    t.bigint "profile_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "slug"
    t.index ["profile_id"], name: "index_portfolios_on_profile_id"
    t.index ["title"], name: "index_portfolios_on_title", unique: true
  end

  create_table "profile_details", force: :cascade do |t|
    t.string "title"
    t.string "content"
    t.integer "sort_order"
    t.bigint "profile_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "icon"
    t.boolean "show_in_contact"
    t.index ["profile_id"], name: "index_profile_details_on_profile_id"
  end

  create_table "profile_links", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.string "url"
    t.string "icon"
    t.integer "sort_order"
    t.bigint "profile_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["profile_id"], name: "index_profile_links_on_profile_id"
  end

  create_table "profiles", force: :cascade do |t|
    t.string "first_name"
    t.string "last_name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "home_photo"
    t.string "profile_photo"
  end

  create_table "services", force: :cascade do |t|
    t.string "name"
    t.text "description"
    t.bigint "profile_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "slug"
    t.string "icon"
    t.index ["name"], name: "index_services_on_name", unique: true
    t.index ["profile_id"], name: "index_services_on_profile_id"
  end

  create_table "skills", force: :cascade do |t|
    t.string "skill_name"
    t.integer "proficiency"
    t.bigint "profile_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["profile_id"], name: "index_skills_on_profile_id"
  end

  create_table "testimonials", force: :cascade do |t|
    t.string "customer_name"
    t.string "customer_position"
    t.text "statement"
    t.string "photo"
    t.bigint "profile_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["profile_id"], name: "index_testimonials_on_profile_id"
  end

  create_table "work_experiences", force: :cascade do |t|
    t.string "job_title"
    t.text "description"
    t.string "employer_name"
    t.string "employer_address"
    t.date "date_started"
    t.date "date_ended"
    t.bigint "profile_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["profile_id"], name: "index_work_experiences_on_profile_id"
  end

  add_foreign_key "educations", "profiles"
  add_foreign_key "portfolio_photos", "portfolios"
  add_foreign_key "portfolios", "profiles"
  add_foreign_key "profile_details", "profiles"
  add_foreign_key "profile_links", "profiles"
  add_foreign_key "services", "profiles"
  add_foreign_key "skills", "profiles"
  add_foreign_key "testimonials", "profiles"
  add_foreign_key "work_experiences", "profiles"
end
