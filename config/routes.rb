Rails.application.routes.draw do
  namespace :admin do
    resources :profiles, :profile_details, :profile_links,
              :work_experiences, :skills, :educations, :services,
              :portfolios, :portfolio_photos, :testimonials

    root to: "profiles#index"
  end
  root :to => 'home#index'

  resources :portfolios, only: %i[index show], param: :slug
  resources :services, only: %i[index show], param: :slug
  resources :blog, param: :slug

  get 'about', to: 'about#index'
  get 'resume', to: 'resume#index'
  get 'contact', to: 'contact#index'
  post 'contact', to: 'contact#create'
  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  get 'logout', to: 'sessions#destroy'
  get 'my-posts', to: 'pages#my_posts'
  get 'register', to: 'users#new'
  post 'register', to: 'users#create'
end
